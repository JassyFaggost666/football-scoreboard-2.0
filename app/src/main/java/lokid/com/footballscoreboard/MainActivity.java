package lokid.com.footballscoreboard;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.util.Locale;

/**
 * Входная точка для приложения.
 *
 * @author lokid
 * @version 1.0
 */
public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    public static final String FIRST_TEAM_NAME = "first_team";
    public static final String SECOND_TEAM_NAME = "second_team";

    public static final String FORMAT_TEXT_VIEW_OF_COUNTER = "%d";

    public static final int VALUE_ZERO = 0;

    Button buttonFirstTeam;
    Button buttonSecondTeam;
    Button buttonResetAllCounters;

    private int counterPointsFirstTeam = VALUE_ZERO;
    private int counterPointsSecondTeam = VALUE_ZERO;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        buttonFirstTeam = (Button) findViewById(R.id.first_team);
        buttonSecondTeam = (Button) findViewById(R.id.second_team);
        buttonResetAllCounters = (Button) findViewById(R.id.reset);


        buttonFirstTeam.setOnClickListener(this);
        buttonSecondTeam.setOnClickListener(this);
        buttonResetAllCounters.setOnClickListener(this);
    }


    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        if (savedInstanceState != null &&
                savedInstanceState.containsKey(FIRST_TEAM_NAME) && savedInstanceState.containsKey(SECOND_TEAM_NAME)) {
            counterPointsFirstTeam = savedInstanceState.getInt(FIRST_TEAM_NAME);
            counterPointsSecondTeam = savedInstanceState.getInt(SECOND_TEAM_NAME);
            outputScoreboard(Team.First);
            outputScoreboard(Team.Second);
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt(FIRST_TEAM_NAME, counterPointsFirstTeam);
        outState.putInt(SECOND_TEAM_NAME, counterPointsSecondTeam);
    }


    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.first_team:
                onClickAddPointTeam(Team.First);
                outputScoreboard(Team.First);

                break;
            case R.id.second_team:
                onClickAddPointTeam(Team.Second);
                outputScoreboard(Team.Second);
                break;
            case R.id.reset:
                onClickResetAll();

                outputScoreboard(Team.First);
                outputScoreboard(Team.Second);

                break;
        }
    }

    /**
     * Обнуляет счётчики команд
     */
    private void onClickResetAll() {
        counterPointsFirstTeam = VALUE_ZERO;
        counterPointsSecondTeam = VALUE_ZERO;
    }

    /**
     * Инкрементирование счёта команды
     *
     * @param team {@link Team} - команда
     */
    private void onClickAddPointTeam(Team team) {
        switch (team) {
            case First:
                counterPointsFirstTeam++;
                break;
            case Second:
                counterPointsSecondTeam++;
                break;
        }
    }

    /**
     * Обновление счётчика на экране
     *
     * @param team {@link Team} - команда
     */
    private void outputScoreboard(Team team) {
        int id = VALUE_ZERO;
        int command = VALUE_ZERO;

        if (team == Team.First) {
            id = R.id.txt_score1;
            command = counterPointsFirstTeam;
        }

        if (team == Team.Second) {
            id = R.id.txt_score2;
            command = counterPointsSecondTeam;
        }


        TextView counterView = (TextView) findViewById(id);
        counterView.setText(String.format(Locale.getDefault(), FORMAT_TEXT_VIEW_OF_COUNTER, command));
    }


}
